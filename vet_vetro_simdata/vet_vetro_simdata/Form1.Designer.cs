
namespace vet_vetro_simdata
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.CS400 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.sendTextBox = new System.Windows.Forms.TextBox();
            this.patientInformationGroup = new System.Windows.Forms.GroupBox();
            this.ageUnitTextBox = new System.Windows.Forms.TextBox();
            this.patientInformationAddButton = new System.Windows.Forms.Button();
            this.ageTextBox = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.patientSexTextBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.patientNameTextBox = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.dataListGroup = new System.Windows.Forms.GroupBox();
            this.finishButton = new System.Windows.Forms.Button();
            this.listLabel10 = new System.Windows.Forms.Label();
            this.listLabel9 = new System.Windows.Forms.Label();
            this.listLabel8 = new System.Windows.Forms.Label();
            this.listLabel7 = new System.Windows.Forms.Label();
            this.listLabel6 = new System.Windows.Forms.Label();
            this.listLabel5 = new System.Windows.Forms.Label();
            this.listLabel4 = new System.Windows.Forms.Label();
            this.listLabel1 = new System.Windows.Forms.Label();
            this.listLabel2 = new System.Windows.Forms.Label();
            this.listLabel3 = new System.Windows.Forms.Label();
            this.resultGroup = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lowPanicRangeTextbox = new System.Windows.Forms.TextBox();
            this.highPanicRangeTextbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lowNormalRangeTextbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.highNormalRangeTextbox = new System.Windows.Forms.TextBox();
            this.resultAddButton = new System.Windows.Forms.Button();
            this.resultAbnormalFlagTextbox = new System.Windows.Forms.TextBox();
            this.resultStatusTextbox = new System.Windows.Forms.TextBox();
            this.resultStatusLabel = new System.Windows.Forms.Label();
            this.unitTextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.dataOrMeasurementValueTextBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.universalTestIDTextBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.testOrderGroup = new System.Windows.Forms.GroupBox();
            this.reportTypeTextBox = new System.Windows.Forms.TextBox();
            this.spacimenDescriptorTextBox = new System.Windows.Forms.TextBox();
            this.piorityTextBox = new System.Windows.Forms.TextBox();
            this.diluentTextBox = new System.Windows.Forms.TextBox();
            this.positionNoTextBox = new System.Windows.Forms.TextBox();
            this.diskNoTextBox = new System.Windows.Forms.TextBox();
            this.sampleNoTextBox = new System.Windows.Forms.TextBox();
            this.sampleIDTextBox = new System.Windows.Forms.TextBox();
            this.testOrderAddButton = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Mythic18 = new System.Windows.Forms.TabPage();
            this.ISE4000 = new System.Windows.Forms.TabPage();
            this.URIT500C = new System.Windows.Forms.TabPage();
            this.connectButton = new System.Windows.Forms.Button();
            this.comLabel = new System.Windows.Forms.Label();
            this.comComboBox = new System.Windows.Forms.ComboBox();
            this.buadrateLable = new System.Windows.Forms.Label();
            this.buadrateComboBox = new System.Windows.Forms.ComboBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.loopCheckBox = new System.Windows.Forms.CheckBox();
            this.loopTextBox = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.CS400.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.patientInformationGroup.SuspendLayout();
            this.dataListGroup.SuspendLayout();
            this.resultGroup.SuspendLayout();
            this.testOrderGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.CS400);
            this.tabControl1.Controls.Add(this.Mythic18);
            this.tabControl1.Controls.Add(this.ISE4000);
            this.tabControl1.Controls.Add(this.URIT500C);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1096, 564);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // CS400
            // 
            this.CS400.Controls.Add(this.groupBox5);
            this.CS400.Controls.Add(this.patientInformationGroup);
            this.CS400.Controls.Add(this.dataListGroup);
            this.CS400.Controls.Add(this.resultGroup);
            this.CS400.Controls.Add(this.testOrderGroup);
            this.CS400.Location = new System.Drawing.Point(4, 22);
            this.CS400.Name = "CS400";
            this.CS400.Padding = new System.Windows.Forms.Padding(3);
            this.CS400.Size = new System.Drawing.Size(1088, 538);
            this.CS400.TabIndex = 0;
            this.CS400.Text = "CS400";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Location = new System.Drawing.Point(266, 337);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(816, 195);
            this.groupBox5.TabIndex = 16;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Serial Monitor";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.textBox1);
            this.groupBox7.Location = new System.Drawing.Point(417, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(393, 170);
            this.groupBox7.TabIndex = 18;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Receive";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(6, 20);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(381, 142);
            this.textBox1.TabIndex = 15;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.sendTextBox);
            this.groupBox6.Location = new System.Drawing.Point(12, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(399, 168);
            this.groupBox6.TabIndex = 17;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Send";
            // 
            // sendTextBox
            // 
            this.sendTextBox.Enabled = false;
            this.sendTextBox.Location = new System.Drawing.Point(6, 20);
            this.sendTextBox.Multiline = true;
            this.sendTextBox.Name = "sendTextBox";
            this.sendTextBox.Size = new System.Drawing.Size(387, 142);
            this.sendTextBox.TabIndex = 15;
            // 
            // patientInformationGroup
            // 
            this.patientInformationGroup.Controls.Add(this.ageUnitTextBox);
            this.patientInformationGroup.Controls.Add(this.patientInformationAddButton);
            this.patientInformationGroup.Controls.Add(this.ageTextBox);
            this.patientInformationGroup.Controls.Add(this.label31);
            this.patientInformationGroup.Controls.Add(this.patientSexTextBox);
            this.patientInformationGroup.Controls.Add(this.label32);
            this.patientInformationGroup.Controls.Add(this.patientNameTextBox);
            this.patientInformationGroup.Controls.Add(this.label33);
            this.patientInformationGroup.Controls.Add(this.label34);
            this.patientInformationGroup.Location = new System.Drawing.Point(266, 25);
            this.patientInformationGroup.Name = "patientInformationGroup";
            this.patientInformationGroup.Size = new System.Drawing.Size(260, 306);
            this.patientInformationGroup.TabIndex = 14;
            this.patientInformationGroup.TabStop = false;
            this.patientInformationGroup.Text = "Patient Information Record (P)";
            // 
            // ageUnitTextBox
            // 
            this.ageUnitTextBox.Location = new System.Drawing.Point(154, 138);
            this.ageUnitTextBox.Name = "ageUnitTextBox";
            this.ageUnitTextBox.Size = new System.Drawing.Size(100, 20);
            this.ageUnitTextBox.TabIndex = 26;
            // 
            // patientInformationAddButton
            // 
            this.patientInformationAddButton.Location = new System.Drawing.Point(179, 277);
            this.patientInformationAddButton.Name = "patientInformationAddButton";
            this.patientInformationAddButton.Size = new System.Drawing.Size(75, 21);
            this.patientInformationAddButton.TabIndex = 10;
            this.patientInformationAddButton.Text = "Add";
            this.patientInformationAddButton.UseVisualStyleBackColor = true;
            this.patientInformationAddButton.Click += new System.EventHandler(this.patientInformationAddButton_Click);
            // 
            // ageTextBox
            // 
            this.ageTextBox.Location = new System.Drawing.Point(154, 103);
            this.ageTextBox.Name = "ageTextBox";
            this.ageTextBox.Size = new System.Drawing.Size(100, 20);
            this.ageTextBox.TabIndex = 25;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 141);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(46, 13);
            this.label31.TabIndex = 3;
            this.label31.Text = "Age unit";
            // 
            // patientSexTextBox
            // 
            this.patientSexTextBox.Location = new System.Drawing.Point(154, 68);
            this.patientSexTextBox.Name = "patientSexTextBox";
            this.patientSexTextBox.Size = new System.Drawing.Size(100, 20);
            this.patientSexTextBox.TabIndex = 24;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 36);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(71, 13);
            this.label32.TabIndex = 0;
            this.label32.Text = "Patient Name";
            // 
            // patientNameTextBox
            // 
            this.patientNameTextBox.Location = new System.Drawing.Point(154, 33);
            this.patientNameTextBox.Name = "patientNameTextBox";
            this.patientNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.patientNameTextBox.TabIndex = 23;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 71);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(61, 13);
            this.label33.TabIndex = 1;
            this.label33.Text = "Patient Sex";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 106);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(26, 13);
            this.label34.TabIndex = 2;
            this.label34.Text = "Age";
            // 
            // dataListGroup
            // 
            this.dataListGroup.Controls.Add(this.finishButton);
            this.dataListGroup.Controls.Add(this.listLabel10);
            this.dataListGroup.Controls.Add(this.listLabel9);
            this.dataListGroup.Controls.Add(this.listLabel8);
            this.dataListGroup.Controls.Add(this.listLabel7);
            this.dataListGroup.Controls.Add(this.listLabel6);
            this.dataListGroup.Controls.Add(this.listLabel5);
            this.dataListGroup.Controls.Add(this.listLabel4);
            this.dataListGroup.Controls.Add(this.listLabel1);
            this.dataListGroup.Controls.Add(this.listLabel2);
            this.dataListGroup.Controls.Add(this.listLabel3);
            this.dataListGroup.Location = new System.Drawing.Point(829, 25);
            this.dataListGroup.Name = "dataListGroup";
            this.dataListGroup.Size = new System.Drawing.Size(253, 306);
            this.dataListGroup.TabIndex = 14;
            this.dataListGroup.TabStop = false;
            this.dataListGroup.Text = "Data List";
            // 
            // finishButton
            // 
            this.finishButton.Location = new System.Drawing.Point(172, 277);
            this.finishButton.Name = "finishButton";
            this.finishButton.Size = new System.Drawing.Size(75, 21);
            this.finishButton.TabIndex = 30;
            this.finishButton.Text = "Finish";
            this.finishButton.UseVisualStyleBackColor = true;
            this.finishButton.Click += new System.EventHandler(this.finishButton_Click);
            // 
            // listLabel10
            // 
            this.listLabel10.AutoSize = true;
            this.listLabel10.Location = new System.Drawing.Point(11, 257);
            this.listLabel10.Name = "listLabel10";
            this.listLabel10.Size = new System.Drawing.Size(10, 13);
            this.listLabel10.TabIndex = 9;
            this.listLabel10.Text = "-";
            // 
            // listLabel9
            // 
            this.listLabel9.AutoSize = true;
            this.listLabel9.Location = new System.Drawing.Point(10, 233);
            this.listLabel9.Name = "listLabel9";
            this.listLabel9.Size = new System.Drawing.Size(10, 13);
            this.listLabel9.TabIndex = 8;
            this.listLabel9.Text = "-";
            // 
            // listLabel8
            // 
            this.listLabel8.AutoSize = true;
            this.listLabel8.Location = new System.Drawing.Point(9, 205);
            this.listLabel8.Name = "listLabel8";
            this.listLabel8.Size = new System.Drawing.Size(10, 13);
            this.listLabel8.TabIndex = 7;
            this.listLabel8.Text = "-";
            // 
            // listLabel7
            // 
            this.listLabel7.AutoSize = true;
            this.listLabel7.Location = new System.Drawing.Point(9, 180);
            this.listLabel7.Name = "listLabel7";
            this.listLabel7.Size = new System.Drawing.Size(10, 13);
            this.listLabel7.TabIndex = 6;
            this.listLabel7.Text = "-";
            // 
            // listLabel6
            // 
            this.listLabel6.AutoSize = true;
            this.listLabel6.Location = new System.Drawing.Point(6, 154);
            this.listLabel6.Name = "listLabel6";
            this.listLabel6.Size = new System.Drawing.Size(10, 13);
            this.listLabel6.TabIndex = 5;
            this.listLabel6.Text = "-";
            // 
            // listLabel5
            // 
            this.listLabel5.AutoSize = true;
            this.listLabel5.Location = new System.Drawing.Point(6, 128);
            this.listLabel5.Name = "listLabel5";
            this.listLabel5.Size = new System.Drawing.Size(10, 13);
            this.listLabel5.TabIndex = 4;
            this.listLabel5.Text = "-";
            // 
            // listLabel4
            // 
            this.listLabel4.AutoSize = true;
            this.listLabel4.Location = new System.Drawing.Point(6, 105);
            this.listLabel4.Name = "listLabel4";
            this.listLabel4.Size = new System.Drawing.Size(10, 13);
            this.listLabel4.TabIndex = 3;
            this.listLabel4.Text = "-";
            // 
            // listLabel1
            // 
            this.listLabel1.AutoSize = true;
            this.listLabel1.ForeColor = System.Drawing.Color.Red;
            this.listLabel1.Location = new System.Drawing.Point(6, 36);
            this.listLabel1.Name = "listLabel1";
            this.listLabel1.Size = new System.Drawing.Size(42, 13);
            this.listLabel1.TabIndex = 0;
            this.listLabel1.Text = "Header";
            // 
            // listLabel2
            // 
            this.listLabel2.AutoSize = true;
            this.listLabel2.Location = new System.Drawing.Point(6, 59);
            this.listLabel2.Name = "listLabel2";
            this.listLabel2.Size = new System.Drawing.Size(10, 13);
            this.listLabel2.TabIndex = 1;
            this.listLabel2.Text = "-";
            // 
            // listLabel3
            // 
            this.listLabel3.AutoSize = true;
            this.listLabel3.Location = new System.Drawing.Point(6, 82);
            this.listLabel3.Name = "listLabel3";
            this.listLabel3.Size = new System.Drawing.Size(10, 13);
            this.listLabel3.TabIndex = 2;
            this.listLabel3.Text = "-";
            // 
            // resultGroup
            // 
            this.resultGroup.Controls.Add(this.label8);
            this.resultGroup.Controls.Add(this.lowPanicRangeTextbox);
            this.resultGroup.Controls.Add(this.highPanicRangeTextbox);
            this.resultGroup.Controls.Add(this.label7);
            this.resultGroup.Controls.Add(this.lowNormalRangeTextbox);
            this.resultGroup.Controls.Add(this.label5);
            this.resultGroup.Controls.Add(this.label20);
            this.resultGroup.Controls.Add(this.highNormalRangeTextbox);
            this.resultGroup.Controls.Add(this.resultAddButton);
            this.resultGroup.Controls.Add(this.resultAbnormalFlagTextbox);
            this.resultGroup.Controls.Add(this.resultStatusTextbox);
            this.resultGroup.Controls.Add(this.resultStatusLabel);
            this.resultGroup.Controls.Add(this.unitTextBox);
            this.resultGroup.Controls.Add(this.label21);
            this.resultGroup.Controls.Add(this.dataOrMeasurementValueTextBox);
            this.resultGroup.Controls.Add(this.label22);
            this.resultGroup.Controls.Add(this.universalTestIDTextBox);
            this.resultGroup.Controls.Add(this.label23);
            this.resultGroup.Controls.Add(this.label24);
            this.resultGroup.Location = new System.Drawing.Point(532, 25);
            this.resultGroup.Name = "resultGroup";
            this.resultGroup.Size = new System.Drawing.Size(291, 306);
            this.resultGroup.TabIndex = 13;
            this.resultGroup.TabStop = false;
            this.resultGroup.Text = "Result Record (R)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(228, 176);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 13);
            this.label8.TabIndex = 37;
            this.label8.Text = "-";
            // 
            // lowPanicRangeTextbox
            // 
            this.lowPanicRangeTextbox.Location = new System.Drawing.Point(185, 173);
            this.lowPanicRangeTextbox.Name = "lowPanicRangeTextbox";
            this.lowPanicRangeTextbox.Size = new System.Drawing.Size(37, 20);
            this.lowPanicRangeTextbox.TabIndex = 38;
            // 
            // highPanicRangeTextbox
            // 
            this.highPanicRangeTextbox.Location = new System.Drawing.Point(244, 173);
            this.highPanicRangeTextbox.Name = "highPanicRangeTextbox";
            this.highPanicRangeTextbox.Size = new System.Drawing.Size(37, 20);
            this.highPanicRangeTextbox.TabIndex = 36;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(228, 141);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "-";
            // 
            // lowNormalRangeTextbox
            // 
            this.lowNormalRangeTextbox.Location = new System.Drawing.Point(185, 138);
            this.lowNormalRangeTextbox.Name = "lowNormalRangeTextbox";
            this.lowNormalRangeTextbox.Size = new System.Drawing.Size(37, 20);
            this.lowNormalRangeTextbox.TabIndex = 35;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Panic Range";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 215);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(107, 13);
            this.label20.TabIndex = 31;
            this.label20.Text = "Result Abnormal Flag";
            // 
            // highNormalRangeTextbox
            // 
            this.highNormalRangeTextbox.Location = new System.Drawing.Point(244, 138);
            this.highNormalRangeTextbox.Name = "highNormalRangeTextbox";
            this.highNormalRangeTextbox.Size = new System.Drawing.Size(37, 20);
            this.highNormalRangeTextbox.TabIndex = 30;
            // 
            // resultAddButton
            // 
            this.resultAddButton.Location = new System.Drawing.Point(210, 277);
            this.resultAddButton.Name = "resultAddButton";
            this.resultAddButton.Size = new System.Drawing.Size(75, 21);
            this.resultAddButton.TabIndex = 10;
            this.resultAddButton.Text = "Add";
            this.resultAddButton.UseVisualStyleBackColor = true;
            this.resultAddButton.Click += new System.EventHandler(this.resultAddButton_Click);
            // 
            // resultAbnormalFlagTextbox
            // 
            this.resultAbnormalFlagTextbox.Location = new System.Drawing.Point(185, 208);
            this.resultAbnormalFlagTextbox.Name = "resultAbnormalFlagTextbox";
            this.resultAbnormalFlagTextbox.Size = new System.Drawing.Size(100, 20);
            this.resultAbnormalFlagTextbox.TabIndex = 29;
            // 
            // resultStatusTextbox
            // 
            this.resultStatusTextbox.Location = new System.Drawing.Point(185, 243);
            this.resultStatusTextbox.Name = "resultStatusTextbox";
            this.resultStatusTextbox.Size = new System.Drawing.Size(100, 20);
            this.resultStatusTextbox.TabIndex = 28;
            // 
            // resultStatusLabel
            // 
            this.resultStatusLabel.AutoSize = true;
            this.resultStatusLabel.Location = new System.Drawing.Point(6, 246);
            this.resultStatusLabel.Name = "resultStatusLabel";
            this.resultStatusLabel.Size = new System.Drawing.Size(70, 13);
            this.resultStatusLabel.TabIndex = 6;
            this.resultStatusLabel.Text = "Result Status";
            // 
            // unitTextBox
            // 
            this.unitTextBox.Location = new System.Drawing.Point(185, 103);
            this.unitTextBox.Name = "unitTextBox";
            this.unitTextBox.Size = new System.Drawing.Size(100, 20);
            this.unitTextBox.TabIndex = 25;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 141);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 13);
            this.label21.TabIndex = 3;
            this.label21.Text = "Normal Range";
            // 
            // dataOrMeasurementValueTextBox
            // 
            this.dataOrMeasurementValueTextBox.Location = new System.Drawing.Point(185, 68);
            this.dataOrMeasurementValueTextBox.Name = "dataOrMeasurementValueTextBox";
            this.dataOrMeasurementValueTextBox.Size = new System.Drawing.Size(100, 20);
            this.dataOrMeasurementValueTextBox.TabIndex = 24;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 36);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(89, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Universal Test ID";
            // 
            // universalTestIDTextBox
            // 
            this.universalTestIDTextBox.Location = new System.Drawing.Point(185, 33);
            this.universalTestIDTextBox.Name = "universalTestIDTextBox";
            this.universalTestIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.universalTestIDTextBox.TabIndex = 23;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 71);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(139, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Data or Measurement Value";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 106);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(26, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Unit";
            // 
            // testOrderGroup
            // 
            this.testOrderGroup.Controls.Add(this.reportTypeTextBox);
            this.testOrderGroup.Controls.Add(this.spacimenDescriptorTextBox);
            this.testOrderGroup.Controls.Add(this.piorityTextBox);
            this.testOrderGroup.Controls.Add(this.diluentTextBox);
            this.testOrderGroup.Controls.Add(this.positionNoTextBox);
            this.testOrderGroup.Controls.Add(this.diskNoTextBox);
            this.testOrderGroup.Controls.Add(this.sampleNoTextBox);
            this.testOrderGroup.Controls.Add(this.sampleIDTextBox);
            this.testOrderGroup.Controls.Add(this.testOrderAddButton);
            this.testOrderGroup.Controls.Add(this.label12);
            this.testOrderGroup.Controls.Add(this.label11);
            this.testOrderGroup.Controls.Add(this.label9);
            this.testOrderGroup.Controls.Add(this.label4);
            this.testOrderGroup.Controls.Add(this.label6);
            this.testOrderGroup.Controls.Add(this.label1);
            this.testOrderGroup.Controls.Add(this.label2);
            this.testOrderGroup.Controls.Add(this.label3);
            this.testOrderGroup.Location = new System.Drawing.Point(6, 25);
            this.testOrderGroup.Name = "testOrderGroup";
            this.testOrderGroup.Size = new System.Drawing.Size(254, 507);
            this.testOrderGroup.TabIndex = 12;
            this.testOrderGroup.TabStop = false;
            this.testOrderGroup.Text = "Test Order Record (O)";
            // 
            // reportTypeTextBox
            // 
            this.reportTypeTextBox.Location = new System.Drawing.Point(148, 264);
            this.reportTypeTextBox.Name = "reportTypeTextBox";
            this.reportTypeTextBox.Size = new System.Drawing.Size(100, 20);
            this.reportTypeTextBox.TabIndex = 22;
            // 
            // spacimenDescriptorTextBox
            // 
            this.spacimenDescriptorTextBox.Location = new System.Drawing.Point(148, 231);
            this.spacimenDescriptorTextBox.Name = "spacimenDescriptorTextBox";
            this.spacimenDescriptorTextBox.Size = new System.Drawing.Size(100, 20);
            this.spacimenDescriptorTextBox.TabIndex = 21;
            // 
            // piorityTextBox
            // 
            this.piorityTextBox.Location = new System.Drawing.Point(148, 198);
            this.piorityTextBox.Name = "piorityTextBox";
            this.piorityTextBox.Size = new System.Drawing.Size(100, 20);
            this.piorityTextBox.TabIndex = 20;
            // 
            // diluentTextBox
            // 
            this.diluentTextBox.Location = new System.Drawing.Point(148, 165);
            this.diluentTextBox.Name = "diluentTextBox";
            this.diluentTextBox.Size = new System.Drawing.Size(100, 20);
            this.diluentTextBox.TabIndex = 16;
            // 
            // positionNoTextBox
            // 
            this.positionNoTextBox.Location = new System.Drawing.Point(148, 132);
            this.positionNoTextBox.Name = "positionNoTextBox";
            this.positionNoTextBox.Size = new System.Drawing.Size(100, 20);
            this.positionNoTextBox.TabIndex = 15;
            // 
            // diskNoTextBox
            // 
            this.diskNoTextBox.Location = new System.Drawing.Point(148, 99);
            this.diskNoTextBox.Name = "diskNoTextBox";
            this.diskNoTextBox.Size = new System.Drawing.Size(100, 20);
            this.diskNoTextBox.TabIndex = 14;
            // 
            // sampleNoTextBox
            // 
            this.sampleNoTextBox.Location = new System.Drawing.Point(148, 66);
            this.sampleNoTextBox.Name = "sampleNoTextBox";
            this.sampleNoTextBox.Size = new System.Drawing.Size(100, 20);
            this.sampleNoTextBox.TabIndex = 13;
            // 
            // sampleIDTextBox
            // 
            this.sampleIDTextBox.Location = new System.Drawing.Point(148, 33);
            this.sampleIDTextBox.Name = "sampleIDTextBox";
            this.sampleIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.sampleIDTextBox.TabIndex = 11;
            // 
            // testOrderAddButton
            // 
            this.testOrderAddButton.Location = new System.Drawing.Point(173, 480);
            this.testOrderAddButton.Name = "testOrderAddButton";
            this.testOrderAddButton.Size = new System.Drawing.Size(75, 21);
            this.testOrderAddButton.TabIndex = 12;
            this.testOrderAddButton.Text = "Add";
            this.testOrderAddButton.UseVisualStyleBackColor = true;
            this.testOrderAddButton.Click += new System.EventHandler(this.testOrderAddButton_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 267);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Report Type";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 234);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Spacimen Descripor";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 201);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Piority";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Diluent";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Position No.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sample ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Sample No.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Disk No.";
            // 
            // Mythic18
            // 
            this.Mythic18.Location = new System.Drawing.Point(4, 22);
            this.Mythic18.Name = "Mythic18";
            this.Mythic18.Padding = new System.Windows.Forms.Padding(3);
            this.Mythic18.Size = new System.Drawing.Size(1088, 538);
            this.Mythic18.TabIndex = 1;
            this.Mythic18.Text = "Mythic18";
            // 
            // ISE4000
            // 
            this.ISE4000.Location = new System.Drawing.Point(4, 22);
            this.ISE4000.Name = "ISE4000";
            this.ISE4000.Size = new System.Drawing.Size(1088, 538);
            this.ISE4000.TabIndex = 2;
            this.ISE4000.Text = "ISE4000";
            // 
            // URIT500C
            // 
            this.URIT500C.Location = new System.Drawing.Point(4, 22);
            this.URIT500C.Name = "URIT500C";
            this.URIT500C.Size = new System.Drawing.Size(1088, 538);
            this.URIT500C.TabIndex = 3;
            this.URIT500C.Text = "URIT500C";
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(310, 586);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 21);
            this.connectButton.TabIndex = 2;
            this.connectButton.Text = "Open";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // comLabel
            // 
            this.comLabel.AutoSize = true;
            this.comLabel.Location = new System.Drawing.Point(13, 589);
            this.comLabel.Name = "comLabel";
            this.comLabel.Size = new System.Drawing.Size(31, 13);
            this.comLabel.TabIndex = 3;
            this.comLabel.Text = "COM";
            // 
            // comComboBox
            // 
            this.comComboBox.FormattingEnabled = true;
            this.comComboBox.Location = new System.Drawing.Point(50, 586);
            this.comComboBox.Name = "comComboBox";
            this.comComboBox.Size = new System.Drawing.Size(76, 21);
            this.comComboBox.TabIndex = 4;
            // 
            // buadrateLable
            // 
            this.buadrateLable.AutoSize = true;
            this.buadrateLable.Location = new System.Drawing.Point(132, 589);
            this.buadrateLable.Name = "buadrateLable";
            this.buadrateLable.Size = new System.Drawing.Size(50, 13);
            this.buadrateLable.TabIndex = 5;
            this.buadrateLable.Text = "Buadrate";
            // 
            // buadrateComboBox
            // 
            this.buadrateComboBox.FormattingEnabled = true;
            this.buadrateComboBox.Location = new System.Drawing.Point(188, 586);
            this.buadrateComboBox.Name = "buadrateComboBox";
            this.buadrateComboBox.Size = new System.Drawing.Size(116, 21);
            this.buadrateComboBox.TabIndex = 6;
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(1029, 586);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 21);
            this.sendButton.TabIndex = 7;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // loopCheckBox
            // 
            this.loopCheckBox.AutoSize = true;
            this.loopCheckBox.Location = new System.Drawing.Point(904, 590);
            this.loopCheckBox.Name = "loopCheckBox";
            this.loopCheckBox.Size = new System.Drawing.Size(50, 17);
            this.loopCheckBox.TabIndex = 8;
            this.loopCheckBox.Text = "Loop";
            this.loopCheckBox.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.loopCheckBox.UseVisualStyleBackColor = true;
            this.loopCheckBox.CheckedChanged += new System.EventHandler(this.loopCheckBox_CheckedChanged);
            // 
            // loopTextBox
            // 
            this.loopTextBox.Enabled = false;
            this.loopTextBox.Location = new System.Drawing.Point(960, 587);
            this.loopTextBox.Name = "loopTextBox";
            this.loopTextBox.Size = new System.Drawing.Size(63, 20);
            this.loopTextBox.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 617);
            this.Controls.Add(this.loopTextBox);
            this.Controls.Add(this.loopCheckBox);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.buadrateComboBox);
            this.Controls.Add(this.buadrateLable);
            this.Controls.Add(this.comComboBox);
            this.Controls.Add(this.comLabel);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.CS400.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.patientInformationGroup.ResumeLayout(false);
            this.patientInformationGroup.PerformLayout();
            this.dataListGroup.ResumeLayout(false);
            this.dataListGroup.PerformLayout();
            this.resultGroup.ResumeLayout(false);
            this.resultGroup.PerformLayout();
            this.testOrderGroup.ResumeLayout(false);
            this.testOrderGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage CS400;
        private System.Windows.Forms.TabPage Mythic18;
        private System.Windows.Forms.TabPage ISE4000;
        private System.Windows.Forms.TabPage URIT500C;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Label comLabel;
        private System.Windows.Forms.ComboBox comComboBox;
        private System.Windows.Forms.Label buadrateLable;
        private System.Windows.Forms.ComboBox buadrateComboBox;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.CheckBox loopCheckBox;
        private System.Windows.Forms.TextBox loopTextBox;
        private System.Windows.Forms.GroupBox testOrderGroup;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox resultGroup;
        private System.Windows.Forms.Label resultStatusLabel;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button resultAddButton;
        private System.Windows.Forms.GroupBox dataListGroup;
        private System.Windows.Forms.Label listLabel10;
        private System.Windows.Forms.Label listLabel9;
        private System.Windows.Forms.Label listLabel8;
        private System.Windows.Forms.Label listLabel7;
        private System.Windows.Forms.Label listLabel6;
        private System.Windows.Forms.Label listLabel5;
        private System.Windows.Forms.Label listLabel4;
        private System.Windows.Forms.Label listLabel1;
        private System.Windows.Forms.Label listLabel2;
        private System.Windows.Forms.Label listLabel3;
        private System.Windows.Forms.GroupBox patientInformationGroup;
        private System.Windows.Forms.TextBox ageUnitTextBox;
        private System.Windows.Forms.Button patientInformationAddButton;
        private System.Windows.Forms.TextBox ageTextBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox patientSexTextBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox patientNameTextBox;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox resultAbnormalFlagTextbox;
        private System.Windows.Forms.TextBox resultStatusTextbox;
        private System.Windows.Forms.TextBox unitTextBox;
        private System.Windows.Forms.TextBox dataOrMeasurementValueTextBox;
        private System.Windows.Forms.TextBox universalTestIDTextBox;
        private System.Windows.Forms.TextBox reportTypeTextBox;
        private System.Windows.Forms.TextBox spacimenDescriptorTextBox;
        private System.Windows.Forms.TextBox piorityTextBox;
        private System.Windows.Forms.TextBox diluentTextBox;
        private System.Windows.Forms.TextBox positionNoTextBox;
        private System.Windows.Forms.TextBox diskNoTextBox;
        private System.Windows.Forms.TextBox sampleNoTextBox;
        private System.Windows.Forms.TextBox sampleIDTextBox;
        private System.Windows.Forms.Button testOrderAddButton;
        private System.Windows.Forms.TextBox sendTextBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button finishButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox lowPanicRangeTextbox;
        private System.Windows.Forms.TextBox highPanicRangeTextbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox lowNormalRangeTextbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox highNormalRangeTextbox;
    }
}

