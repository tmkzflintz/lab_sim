using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using vet_vetro_simdata;
using System.IO.Ports;
using System.Collections.Generic;
using System.Threading;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace vet_vetro_simdata
{
    public partial class Form1 : Form
    {
        string[] ArrayComPortsNames = null;
        int index = -1;
        string ComPortName = null;
        string InputData = String.Empty;
        delegate void SetTextCallback(string text);
        CS400 cs400Packet;
        int recordSum = 1;
        int sequence = 1;
        List<string> cs400List;
        List<string> packet;
        List<Label> labelsList;
        SerialPort sp;
        Thread writeThread;

        public Form1()
        {
            InitializeComponent();
            readComPort();
            recommendBuadrate();            
            sp = new SerialPort();
            cs400Packet = new CS400();

            cs400List = new List<string>();
            packet = new List<string>();
            labelsList = new List<Label>();

            labelsList.Add(listLabel1);
            labelsList.Add(listLabel2);
            labelsList.Add(listLabel3);
            labelsList.Add(listLabel4);
            labelsList.Add(listLabel5);
            labelsList.Add(listLabel6);
            labelsList.Add(listLabel7);
            labelsList.Add(listLabel8);
            labelsList.Add(listLabel9);
            labelsList.Add(listLabel10);

            testOrderGroup.Enabled = false;
            resultGroup.Enabled = false;
            
        }

        private void readComPort()
        {
            ArrayComPortsNames = SerialPort.GetPortNames();
            ComPortName = null;
            index = -1;
            do
            {
                index += 1;
                comComboBox.Items.Add(ArrayComPortsNames[index]);
            }

            while (!((ArrayComPortsNames[index] == ComPortName)
                          || (index == ArrayComPortsNames.GetUpperBound(0))));
            Array.Sort(ArrayComPortsNames);

            if (index == ArrayComPortsNames.GetUpperBound(0))
            {
                ComPortName = ArrayComPortsNames[0];
            }
            comComboBox.Text = ArrayComPortsNames[0];
        }

        private void recommendBuadrate()
        {
            buadrateComboBox.Items.Add(9600);
            buadrateComboBox.Items.Add(14400);
            buadrateComboBox.Items.Add(19200);
            buadrateComboBox.Items.Add(38400);
            buadrateComboBox.Items.Add(57600);
            buadrateComboBox.Items.Add(115200);
            buadrateComboBox.Items.ToString();

            buadrateComboBox.Text = buadrateComboBox.Items[0].ToString();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = tabControl1.SelectedIndex;
            if (index == 0 || index == 1 || index == 2)
            {
                comLabel.Text = "COM";
                comLabel.Visible = true;
                buadrateLable.Text = "Buadrate";
                comComboBox.Visible = true;
                buadrateComboBox.Visible = true;
                connectButton.Visible = true;
            }
            else if (index == 3)
            {
                comLabel.Visible = false;
                buadrateLable.Text = "Port";
                comComboBox.Visible = false;
                buadrateComboBox.Visible = true;
                connectButton.Visible = true;
            }
            
        }

        private void loopCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if(loopCheckBox.Checked)
            {
                loopTextBox.Enabled = true;
            }
            else
            {
                loopTextBox.Enabled = false;
            }
        }

        private void patientInformationAddButton_Click(object sender, EventArgs e)
        {
                                            
            cs400List.Clear();
            

            cs400Packet.SenderName  = "CS400";
            cs400Packet.ReceiverID  = "Host";
            cs400Packet.VersionNo   = "1";

            cs400List.Add("Header");            
            packet.Add(cs400Packet.HeaderPacket(recordSum % 8));
            labelsList[recordSum - 1].Text = cs400List[recordSum - 1];
            labelsList[recordSum - 1].ForeColor = Color.Red;
            recordSum++;
            
            
            cs400Packet.PatientName = patientNameTextBox.Text;
            cs400Packet.PatientSex  = patientSexTextBox.Text;
            cs400Packet.Age         = ageTextBox.Text;
            cs400Packet.AgeUnit     = ageUnitTextBox.Text;

            cs400List.Add("Patient Information");
            packet.Add(cs400Packet.PatientInformationPacket(recordSum % 8, sequence));
            labelsList[recordSum - 1].Text = cs400List[recordSum - 1];
            labelsList[recordSum - 1].ForeColor = Color.Red;
            recordSum++;

            testOrderGroup.Enabled = true;
            patientInformationGroup.Enabled = false;
            resultGroup.Enabled = false;
        }

        private void testOrderAddButton_Click(object sender, EventArgs e)
        {
            cs400Packet.SampleID = sampleIDTextBox.Text;
            cs400Packet.SampleNo = sampleNoTextBox.Text;
            cs400Packet.DiskNo = diskNoTextBox.Text;
            cs400Packet.PositionNo = positionNoTextBox.Text;
            cs400Packet.Diluent = diluentTextBox.Text;
            cs400Packet.Piority = piorityTextBox.Text;
            cs400Packet.SpacimeDescriptor = spacimenDescriptorTextBox.Text;
            cs400Packet.ReportType = reportTypeTextBox.Text;

            cs400List.Add("Test Order");
            packet.Add(cs400Packet.TestOrderPacket(recordSum % 8, sequence));
            labelsList[recordSum - 1].Text = cs400List[recordSum - 1];
            labelsList[recordSum - 1].ForeColor = Color.Red;
            recordSum++;

            testOrderGroup.Enabled = false;
            patientInformationGroup.Enabled = false;
            resultGroup.Enabled = true;
        }

        private void resultAddButton_Click(object sender, EventArgs e)
        {
            cs400Packet.UniversalTestID = universalTestIDTextBox.Text;
            cs400Packet.DataOrMeasurementValue = dataOrMeasurementValueTextBox.Text;
            cs400Packet.Unit = unitTextBox.Text;
            cs400Packet.LowNormalRange = lowNormalRangeTextbox.Text;
            cs400Packet.HighNormalRange = highNormalRangeTextbox.Text;
            cs400Packet.LowPanicRange = lowPanicRangeTextbox.Text;
            cs400Packet.HighPanicRange = highPanicRangeTextbox.Text;
            cs400Packet.ResultAbnormalFlag = resultAbnormalFlagTextbox.Text;
            cs400Packet.ResultStatus = resultStatusTextbox.Text;

            cs400List.Add("Result of " + cs400Packet.UniversalTestID);
            packet.Add(cs400Packet.ResultPacket(recordSum % 8, sequence));
            labelsList[recordSum - 1].Text = cs400List[recordSum - 1];
            labelsList[recordSum - 1].ForeColor = Color.Red;
            recordSum++;
            sequence++;            
        }

        private void finishButton_Click(object sender, EventArgs e)
        {
            sequence = 1;
            cs400Packet.TerminationCode = "N";

            cs400List.Add("Terminate");
            packet.Add(cs400Packet.TerminatorPacket(recordSum % 8, sequence));
            labelsList[recordSum - 1].Text = cs400List[recordSum - 1];
            labelsList[recordSum - 1].ForeColor = Color.Red;
            recordSum = 1;

            testOrderGroup.Enabled = false;
            patientInformationGroup.Enabled = false;
            resultGroup.Enabled = false;

            finishButton.Enabled = false;
        }



        private void connectButton_Click(object sender, EventArgs e)
        {
            if (connectButton.Text == "Open")
            {
                connectButton.Text = "Close";
                sp.PortName = Convert.ToString(comComboBox.Text);
                sp.BaudRate = Convert.ToInt32(buadrateComboBox.Text);             
                sp.DataReceived += sp_DataReceived;
                sp.Open();
                comComboBox.Enabled = false;
                buadrateComboBox.Enabled = false;
            }
            else if (connectButton.Text == "Close")
            {
                connectButton.Text = "Open";                
                sp.Close();
                comComboBox.Enabled = true;
                buadrateComboBox.Enabled = true;
            }
        }

        

        private void SetText(string text)
        {
            sendTextBox.Text += text;
        }

        private void sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort spL = (SerialPort)sender;
            InputData = spL.ReadExisting();
            if (InputData != String.Empty)
            {
                BeginInvoke(new SetTextCallback(SetText), new object[] { InputData });
            }
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            sendButton.Enabled = false;
            WriteThread();

            sendButton.Enabled = true;
            finishButton.Enabled = true;

        }

        private void WriteThread()
        {            
            foreach (string s in packet)
            {
                while (!InputData.Equals(0x06)) ;
                sp.Write(s);                
            }
        }
    }
}
