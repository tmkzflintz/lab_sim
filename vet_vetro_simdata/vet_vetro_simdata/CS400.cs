﻿using System;

namespace vet_vetro_simdata
{
    public class CS400
    {

        static string soh = char.ConvertFromUtf32(1);
        static string stx = char.ConvertFromUtf32(2);
        static string etx = char.ConvertFromUtf32(3);
        static string eot = char.ConvertFromUtf32(4);
        static string enq = char.ConvertFromUtf32(5);
        static string ack = char.ConvertFromUtf32(6);
        static string nack = char.ConvertFromUtf32(21);
        static string etb = char.ConvertFromUtf32(23);
        static string lf = char.ConvertFromUtf32(10);
        static string cr = char.ConvertFromUtf32(13);

        const char pipe = '|';
        const char caret = '^';
        const char back_slash = '\\';

        // Header Properties
        string delimiterDef = @"|\^&";
        
        // Header Properties get set
        public string SenderName { get; set; } = String.Empty;
        public string ReceiverID { get; set; } = String.Empty;
        public string VersionNo { get; set; } = String.Empty;

        // Patient Information get set
        public string PatientName { get; set; } = String.Empty;
        public string PatientSex { get; set; } = String.Empty;
        public string Age { get; set; } = String.Empty;
        public string AgeUnit { get; set; } = String.Empty;

        // Test Order get set
        public string SampleID { get; set; } = String.Empty;
        public string SampleNo { get; set; } = String.Empty;
        public string DiskNo { get; set; } = String.Empty;
        public string PositionNo { get; set; } = String.Empty;
        public string Diluent { get; set; } = String.Empty;
        public string Piority { get; set; } = String.Empty;
        public string SpacimeDescriptor { get; set; } = String.Empty;
        public string ReportType { get; set; } = String.Empty;

        // Result get set
        public string UniversalTestID { get; set; } = String.Empty;
        public string DataOrMeasurementValue { get; set; } = String.Empty;
        public string Unit { get; set; } = String.Empty;
        public string LowNormalRange { get; set; } = String.Empty;
        public string HighNormalRange { get; set; } = String.Empty;
        public string LowPanicRange { get; set; } = String.Empty;
        public string HighPanicRange { get; set; } = String.Empty;
        public string ResultAbnormalFlag { get; set; } = String.Empty;
        public string NatureOfAbnormalityTesting { get; set; } = String.Empty;
        public string ResultStatus { get; set; } = String.Empty;

        // Terminator get set        
        public string TerminationCode { get; set; } = String.Empty;

        public CS400()
        {

        }

        private string CheckSum(string s)
        {
            Byte checksum = 0;
            foreach (char c in s)
            {
                checksum += (Byte) c;
            }
            return checksum.ToString("X2");
        }

        private string Packaging(string record)
        {
            string strSum = String.Empty;
            record = record + cr + etx;
            strSum = CheckSum(record);
            record = stx + record + strSum;
            return record;
        }

        public string HeaderPacket(int recordNumber)
        {
            string record = String.Empty;
            record = recordNumber + "H" + pipe + delimiterDef + new String(pipe, 3) + SenderName + new String(pipe, 5) + ReceiverID + new String(pipe, 3) + VersionNo + pipe + DateTime.Now.ToString("yyyyMMddhhmmss");

            return Packaging(record);
        }

        public string PatientInformationPacket(int recordNumber, int sequenceNumber)
        {
            string record = String.Empty;
            record = recordNumber + "P" + pipe + sequenceNumber + new String(pipe, 4) + PatientName + new String(pipe, 3) + PatientSex + new String(pipe, 6) + Age + caret + AgeUnit;

            return Packaging(record);
        }

        public string TestOrderPacket(int recordNumber, int sequenceNumber)
        {
            string record = String.Empty;
            record = recordNumber + "O" + pipe + sequenceNumber + pipe + SampleID + caret + SampleNo + caret + DiskNo + caret + PositionNo + caret + Diluent + new String(pipe, 3) + Piority + pipe + DateTime.Now.ToString("yyyyMMddhhmmss") + new String(pipe, 9) + SpacimeDescriptor + new String(pipe, 10) + ReportType;
            return Packaging(record);
        }

        public string ResultPacket(int recordNumber, int sequenceNumber)
        {
            string record = String.Empty;
            record = recordNumber + "R" + pipe + sequenceNumber + pipe + new String(caret, 3) + UniversalTestID + pipe + DataOrMeasurementValue + pipe + Unit + pipe + LowNormalRange + caret + HighNormalRange + back_slash + LowPanicRange + caret + HighPanicRange + pipe + ResultAbnormalFlag + pipe + NatureOfAbnormalityTesting + pipe + ResultStatus + new String(pipe, 4) + DateTime.Now.ToString("yyyyMMddhhmmss");
            return Packaging(record);
        }

        public string TerminatorPacket(int recordNumber, int sequenceNumber)
        {
            string record = String.Empty;
            record = recordNumber + "L" + pipe + sequenceNumber + pipe + TerminationCode;
            return Packaging(record);
        }

        //public string[] SendOrder(List<string> tests)
        //{
        //    string[] send = new string[6];

        //    send[0] = enq;
        //    send[1] = Header();
        //    send[2] = Patient(_barcode, 30, 'M');
        //    send[3] = Order(_barcode, tests);
        //    send[4] = Terminator();
        //    send[5] = eot;

        //    return send;
        //} 
    }
}
